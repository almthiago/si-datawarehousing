-- -----------------------------------------------------
-- Table tb_aux_canal
-- -----------------------------------------------------
CREATE  TABLE tb_aux_canal (
  data_carga DATETIME NOT NULL ,
  id_canal INT NULL ,
  nome_canal VARCHAR(45) NULL );


-- -----------------------------------------------------
-- Table tb_aux_localizacao
-- -----------------------------------------------------
CREATE  TABLE tb_aux_localizacao (
  data_carga DATETIME NOT NULL ,
  id_localizacao INT NULL ,
  bairro VARCHAR(45) NULL ,
  cidade VARCHAR(45) NULL ,
  estado VARCHAR(45) NULL );


-- -----------------------------------------------------
-- Table tb_aux_tipo_transacao
-- -----------------------------------------------------
CREATE  TABLE tb_aux_tipo_transacao (
  data_carga DATETIME NOT NULL ,
  id_tipo_transacao INT NULL ,
  tipo_transacao VARCHAR(45) NULL );


-- -----------------------------------------------------
-- Table tb_aux_transacao_programa
-- -----------------------------------------------------
CREATE  TABLE tb_aux_transacao_programa (
  data_carga DATETIME NOT NULL ,
  id_transacao INT NULL ,
  id_tipo_transacao INT NULL ,
  id_tipo_pagamento INT NULL ,
  data_transacao DATETIME NULL ,
  id_localizacao INT NULL ,
  id_cliente INT NULL ,
  id_faixa_etaria INT NULL ,
  id_programa INT NULL ,
  id_categoria_programa INT NULL ,
  id_promocao INT NULL ,
  quantidade INT NULL ,
  valor NUMERIC(10,2) NULL);


-- -----------------------------------------------------
-- Table tb_aux_categoria_canal
-- -----------------------------------------------------
CREATE  TABLE tb_aux_categoria_canal (
  data_carga DATETIME NOT NULL ,
  id_categoria_canal INT NULL ,
  categoria_canal VARCHAR(45) NULL );


-- -----------------------------------------------------
-- Table tb_aux_categoria_programa
-- -----------------------------------------------------
CREATE  TABLE tb_aux_categoria_programa (
  data_carga DATETIME NOT NULL ,
  id_categoria_programa INT NULL ,
  categoria_programa VARCHAR(250) NULL );


-- -----------------------------------------------------
-- Table tb_aux_cliente
-- -----------------------------------------------------
CREATE  TABLE tb_aux_cliente (
  data_carga DATETIME NOT NULL ,
  id_cliente INT NULL ,
  sexo VARCHAR(45) NULL );


-- -----------------------------------------------------
-- Table tb_aux_faixa_etaria
-- -----------------------------------------------------
CREATE  TABLE tb_aux_faixa_etaria (
  data_carga DATETIME NOT NULL ,
  id_faixa_etaria INT NULL ,
  faixa_etaria VARCHAR(45) NULL );


-- -----------------------------------------------------
-- Table tb_aux_pacote
-- -----------------------------------------------------
CREATE  TABLE tb_aux_pacote (
  data_carga DATETIME NOT NULL ,
  id_pacote INT NULL ,
  nome_pacote VARCHAR(250) NULL );


-- -----------------------------------------------------
-- Table tb_aux_programa
-- -----------------------------------------------------
CREATE  TABLE tb_aux_programa (
  data_carga DATETIME NOT NULL ,
  id_programa INT NULL ,
  nome_programa VARCHAR(250) NULL );


-- -----------------------------------------------------
-- Table tb_aux_promocao
-- -----------------------------------------------------
CREATE  TABLE tb_aux_promocao (
  data_carga DATETIME NOT NULL ,
  id_promocao INT NULL ,
  promocao VARCHAR(45) NULL );


-- -----------------------------------------------------
-- Table tb_aux_tipo_pagamento
-- -----------------------------------------------------
CREATE  TABLE tb_aux_tipo_pagamento (
  data_carga DATETIME NOT NULL ,
  id_tipo_pagamento INT NULL ,
  tipo_pagamento VARCHAR(45) NULL );


-- -----------------------------------------------------
-- Table tb_aux_pagamento_canal
-- -----------------------------------------------------
CREATE  TABLE tb_aux_pagamento_canal (
  data_carga DATETIME NOT NULL ,
  id_pagamento INT NULL ,
  id_tipo_transacao INT NULL ,
  id_tipo_pagamento INT NULL ,
  id_promocao INT NULL ,
  id_canal INT NULL ,
  id_categoria_canal INT NULL ,
  data_pagamento DATETIME NULL ,
  id_localizacao INT NULL ,
  id_cliente INT NULL ,
  id_faixa_etaria INT NULL ,
  referente_ao_mes DATETIME NULL ,
  valor NUMERIC(10,2) NULL,
  quantidade INT NULL);


-- -----------------------------------------------------
-- Table tb_aux_pagamento_pacote
-- -----------------------------------------------------
CREATE  TABLE tb_aux_pagamento_pacote (
  data_carga DATETIME NOT NULL ,
  id_pagamento INT NULL ,
  id_tipo_transacao INT NULL ,
  id_tipo_pagamento INT NULL ,
  id_promocao INT NULL ,
  id_pacote INT NULL ,
  data_pagamento DATETIME NULL ,
  id_localizacao INT NULL ,
  id_cliente INT NULL ,
  id_faixa_etaria INT NULL ,
  referente_ao_mes DATETIME NULL ,
  valor NUMERIC(10,2) NULL,
  quantidade INT NULL);


-- -----------------------------------------------------
-- Table tb_aux_pagamento_programa
-- -----------------------------------------------------
CREATE  TABLE tb_aux_pagamento_programa (
  data_carga DATETIME NOT NULL ,
  id_pagamento INT NULL ,
  id_tipo_transacao INT NULL ,
  id_tipo_pagamento INT NULL ,
  id_promocao INT NULL ,
  id_programa INT NULL ,
  id_categoria_programa INT NULL ,
  data_pagamento DATETIME NULL ,
  id_localizacao INT NULL ,
  id_cliente INT NULL ,
  id_faixa_etaria INT NULL ,
  referente_ao_mes DATETIME NULL ,
  valor NUMERIC(10,2) NULL,
  quantidade INT NULL);


-- -----------------------------------------------------
-- Table tb_aux_transacao_canal
-- -----------------------------------------------------
CREATE  TABLE tb_aux_transacao_canal (
  data_carga DATETIME NOT NULL ,
  id_transacao INT NULL ,
  id_tipo_transacao INT NULL ,
  id_tipo_pagamento INT NULL ,
  id_promocao INT NULL ,
  id_canal INT NULL ,
  id_categoria_canal INT NULL ,
  data_transacao DATETIME NULL ,
  id_localizacao INT NULL ,
  id_cliente INT NULL ,
  id_faixa_etaria INT NULL ,
  quantidade INT NULL ,
  valor NUMERIC(10,2) NULL );


-- -----------------------------------------------------
-- Table tb_aux_transacao_pacote
-- -----------------------------------------------------
CREATE  TABLE tb_aux_transacao_pacote (
  data_carga DATETIME NOT NULL ,
  id_transacao INT NULL ,
  id_tipo_transacao INT NULL ,
  id_tipo_pagamento INT NULL ,
  data_transacao DATETIME NULL ,
  id_faixa_etaria INT NULL ,
  id_cliente INT NULL ,
  id_localizacao INT NULL ,
  id_promocao INT NULL ,
  id_pacote INT NULL ,
  valor NUMERIC(10,2) NULL ,
  quantidade INT NULL );

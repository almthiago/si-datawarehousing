-- -----------------------------------------------------
-- Table tb_faixa_etaria
-- -----------------------------------------------------
CREATE  TABLE tb_faixa_etaria (
  id_faixa_etaria INT NOT NULL IDENTITY ,
  faixa_etaria VARCHAR(45) NOT NULL ,
  PRIMARY KEY (id_faixa_etaria) );


-- -----------------------------------------------------
-- Table tb_cliente
-- -----------------------------------------------------
CREATE  TABLE tb_cliente (
  id_cliente INT NOT NULL IDENTITY ,
  nome VARCHAR(100) NOT NULL ,
  sexo CHAR(1) NOT NULL ,
  id_faixa_etaria INT NOT NULL ,
  PRIMARY KEY (id_cliente) ,
    FOREIGN KEY (id_faixa_etaria )
    REFERENCES tb_faixa_etaria (id_faixa_etaria ));


-- -----------------------------------------------------
-- Table tb_moradia
-- -----------------------------------------------------
CREATE  TABLE tb_moradia (
  id_moradia INT NOT NULL IDENTITY ,
  numero VARCHAR(5) NULL ,
  bairro VARCHAR(45) NOT NULL ,
  cidade VARCHAR(45) NOT NULL ,
  estado CHAR(2) NOT NULL ,
  id_cliente INT NOT NULL ,
  PRIMARY KEY (id_moradia) ,
    FOREIGN KEY (id_cliente )
    REFERENCES tb_cliente (id_cliente ));


-- -----------------------------------------------------
-- Table tb_tipo_transacao
-- -----------------------------------------------------
CREATE  TABLE tb_tipo_transacao (
  id_tipo_transacao INT NOT NULL IDENTITY ,
  tipo_transacao VARCHAR(45) NOT NULL ,
  PRIMARY KEY (id_tipo_transacao) );


-- -----------------------------------------------------
-- Table tb_tipo_pagamento
-- -----------------------------------------------------
CREATE  TABLE tb_tipo_pagamento (
  id_tipo_pagamento INT NOT NULL IDENTITY ,
  tipo_pagamento VARCHAR(45) NOT NULL ,
  PRIMARY KEY (id_tipo_pagamento) );


-- -----------------------------------------------------
-- Table tb_promocao
-- -----------------------------------------------------
CREATE  TABLE tb_promocao (
  id_promocao INT NOT NULL IDENTITY ,
  descricao VARCHAR(500) NOT NULL ,
  titulo VARCHAR(45) NOT NULL ,
  desconto NUMERIC(10,2) NULL ,
  PRIMARY KEY (id_promocao) );


-- -----------------------------------------------------
-- Table tb_pacote
-- -----------------------------------------------------
CREATE  TABLE tb_pacote (
  id_pacote INT NOT NULL IDENTITY ,
  nome VARCHAR(250) NOT NULL,
  PRIMARY KEY (id_pacote) );


-- -----------------------------------------------------
-- Table tb_produto
-- -----------------------------------------------------
CREATE  TABLE tb_produto (
  id_produto INT NOT NULL IDENTITY ,
  nome VARCHAR(250) NOT NULL ,
  id_promocao INT NOT NULL ,
  id_pacote INT NOT NULL ,
  valor NUMERIC(10,2) NOT NULL ,
  PRIMARY KEY (id_produto) ,
    FOREIGN KEY (id_promocao )
    REFERENCES tb_promocao (id_promocao ),
    FOREIGN KEY (id_pacote )
    REFERENCES tb_pacote (id_pacote ));


-- -----------------------------------------------------
-- Table tb_transacao
-- -----------------------------------------------------
CREATE  TABLE tb_transacao (
  id_transacao INT NOT NULL IDENTITY ,
  id_moradia INT NOT NULL ,
  id_produto INT NOT NULL ,
  id_tipo_transacao INT NOT NULL ,
  id_tipo_pagamento INT NOT NULL ,
  data DATETIME NOT NULL ,
  valor_total VARCHAR(45) NOT NULL ,
  motivo VARCHAR(250) NULL ,
  qtde_mensalidades INT NOT NULL ,
  valor_mensal NUMERIC(10,2) NULL ,
  PRIMARY KEY (id_transacao) ,
    FOREIGN KEY (id_moradia )
    REFERENCES tb_moradia (id_moradia ),
    FOREIGN KEY (id_produto )
    REFERENCES tb_produto (id_produto ),
    FOREIGN KEY (id_tipo_transacao )
    REFERENCES tb_tipo_transacao (id_tipo_transacao ),
    FOREIGN KEY (id_tipo_pagamento )
    REFERENCES tb_tipo_pagamento (id_tipo_pagamento ));


-- -----------------------------------------------------
-- Table tb_categoria_canal
-- -----------------------------------------------------
CREATE  TABLE tb_categoria_canal (
  id_categoria_canal INT NOT NULL IDENTITY ,
  categoria_canal VARCHAR(45) NOT NULL ,
  PRIMARY KEY (id_categoria_canal) );


-- -----------------------------------------------------
-- Table tb_canal
-- -----------------------------------------------------
CREATE  TABLE tb_canal (
  id_canal INT NOT NULL IDENTITY(2,2) ,
  nome VARCHAR(45) NOT NULL ,
  id_categoria_canal INT NOT NULL ,
  valor NUMERIC(10,2) NOT NULL ,
  PRIMARY KEY (id_canal) ,
    FOREIGN KEY (id_categoria_canal )
    REFERENCES tb_categoria_canal (id_categoria_canal ));


-- -----------------------------------------------------
-- Table tb_categoria_programa
-- -----------------------------------------------------
CREATE  TABLE tb_categoria_programa (
  id_categoria_programa INT NOT NULL IDENTITY ,
  categoria_programa VARCHAR(45) NOT NULL ,
  PRIMARY KEY (id_categoria_programa) );


-- -----------------------------------------------------
-- Table tb_programa
-- -----------------------------------------------------
CREATE  TABLE tb_programa (
  id_programa INT NOT NULL IDENTITY(1,2) ,
  id_canal INT NOT NULL ,
  nome VARCHAR(250) NOT NULL ,
  id_categoria_programa INT NOT NULL ,
  valor NUMERIC(10,2) NOT NULL ,
  PRIMARY KEY (id_programa) ,
    FOREIGN KEY (id_canal )
    REFERENCES tb_canal (id_canal ),
	FOREIGN KEY (id_categoria_programa )
    REFERENCES tb_categoria_programa (id_categoria_programa ));


-- -----------------------------------------------------
-- Table tb_unidade_pacote
-- -----------------------------------------------------
CREATE  TABLE tb_unidade_pacote (
  id_unidade_pacote INT NOT NULL IDENTITY ,
  id_canal_ou_programa INT NOT NULL ,
  PRIMARY KEY (id_unidade_pacote));


-- -----------------------------------------------------
-- Table tb_unidade_pacote_has_tb_pacote
-- -----------------------------------------------------
CREATE  TABLE tb_unidade_pacote_has_tb_pacote (
  id_unidade_pacote INT NOT NULL ,
  id_pacote INT NOT NULL ,
  PRIMARY KEY (id_unidade_pacote, id_pacote) ,
    FOREIGN KEY (id_unidade_pacote )
    REFERENCES tb_unidade_pacote (id_unidade_pacote ),
	FOREIGN KEY (id_pacote )
    REFERENCES tb_pacote (id_pacote ));
    
    
-- -----------------------------------------------------
-- Table tb_pagamento
-- -----------------------------------------------------
CREATE  TABLE tb_pagamento (
  id_pagamento INT NOT NULL IDENTITY ,
  id_transacao INT NOT NULL ,
  valor NUMERIC(10,2) NOT NULL ,
  data_pagamento DATETIME NULL ,
  referente_ao_mes DATETIME NOT NULL ,
  fl_pago CHAR(1) NOT NULL ,
  PRIMARY KEY (id_pagamento) ,
    FOREIGN KEY (id_transacao )
    REFERENCES tb_transacao (id_transacao ));


-- -----------------------------------------------------
-- Table tb_valor_programa
-- -----------------------------------------------------
CREATE  TABLE tb_valor_programa (
  id_valor_programa INT NOT NULL IDENTITY ,
  id_programa INT NOT NULL ,
  valor NUMERIC(10,2) NOT NULL ,
  data_inicio DATETIME NOT NULL ,
  data_fim DATETIME NULL ,
  PRIMARY KEY (id_valor_programa) ,
    FOREIGN KEY (id_programa )
    REFERENCES tb_programa (id_programa ));


-- -----------------------------------------------------
-- Table tb_valor_canal
-- -----------------------------------------------------
CREATE  TABLE tb_valor_canal (
  id_valor_canal INT NOT NULL IDENTITY ,
  id_canal INT NOT NULL ,
  valor NUMERIC(10,2) NOT NULL ,
  data_inicio DATETIME NOT NULL ,
  data_fim DATETIME NULL ,
  PRIMARY KEY (id_valor_canal) ,
    FOREIGN KEY (id_canal )
    REFERENCES tb_canal (id_canal ));

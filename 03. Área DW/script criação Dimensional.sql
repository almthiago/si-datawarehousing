-- -----------------------------------------------------
-- Table dim_tipo_transacao
-- -----------------------------------------------------
CREATE  TABLE dim_tipo_transacao (
  cod_tipo_transacao INT NOT NULL IDENTITY ,
  id_tipo_transacao INT NOT NULL ,
  tipo_transacao VARCHAR(45) NOT NULL ,
  data_inicio DATETIME NULL ,
  data_fim DATETIME NULL ,
  fl_corrente CHAR(1) NULL ,
  PRIMARY KEY (cod_tipo_transacao) );


-- -----------------------------------------------------
-- Table dim_tipo_pagamento
-- -----------------------------------------------------
CREATE  TABLE dim_tipo_pagamento (
  cod_tipo_pagamento INT NOT NULL IDENTITY ,
  id_tipo_pagamento INT NOT NULL ,
  tipo_pagamento VARCHAR(45) NOT NULL ,
  data_inicio DATETIME NULL ,
  data_fim DATETIME NULL ,
  fl_corrente CHAR(1) NULL ,
  PRIMARY KEY (cod_tipo_pagamento) );


-- -----------------------------------------------------
-- Table dim_promocao
-- -----------------------------------------------------
CREATE  TABLE dim_promocao (
  cod_promocao INT NOT NULL IDENTITY ,
  id_promocao INT NOT NULL ,
  promocao VARCHAR(45) NOT NULL ,
  data_inicio DATETIME NULL ,
  data_fim DATETIME NULL ,
  fl_corrente CHAR(1) NULL ,
  PRIMARY KEY (cod_promocao) );


-- -----------------------------------------------------
-- Table dim_localizacao
-- -----------------------------------------------------
CREATE  TABLE dim_localizacao (
  cod_localizacao INT NOT NULL IDENTITY ,
  id_localizacao INT NOT NULL ,
  bairro VARCHAR(45) NOT NULL ,
  cidade VARCHAR(45) NOT NULL ,
  estado VARCHAR(45) NOT NULL ,
  data_inicio DATETIME NULL ,
  data_fim DATETIME NULL ,
  fl_corrente CHAR(1) NULL ,
  PRIMARY KEY (cod_localizacao) );


-- -----------------------------------------------------
-- Table dim_cliente
-- -----------------------------------------------------
CREATE  TABLE dim_cliente (
  cod_cliente INT NOT NULL IDENTITY ,
  id_cliente INT NOT NULL ,
  sexo VARCHAR(45) NOT NULL ,
  data_inicio DATETIME NULL ,
  data_fim DATETIME NULL ,
  fl_corrente CHAR(1) NULL ,
  PRIMARY KEY (cod_cliente) );


-- -----------------------------------------------------
-- Table dim_faixa_etaria
-- -----------------------------------------------------
CREATE  TABLE dim_faixa_etaria (
  cod_faixa_etaria INT NOT NULL IDENTITY ,
  id_faixa_etaria INT NOT NULL ,
  faixa_etaria VARCHAR(45) NOT NULL ,
  data_inicio DATETIME NULL ,
  data_fim DATETIME NULL ,
  fl_corrente CHAR(1) NULL ,
  PRIMARY KEY (cod_faixa_etaria) );


-- -----------------------------------------------------
-- Table dim_tempo
-- -----------------------------------------------------
CREATE  TABLE dim_tempo (
  cod_tempo INT NOT NULL IDENTITY ,
  nivel VARCHAR(25) NOT NULL ,
  data DATETIME NULL ,
  dia VARCHAR(45) NULL ,
  dia_semana VARCHAR(45) NULL ,
  dia_util CHAR(3) NULL ,
  fim_semana CHAR(3) NULL ,
  quinzena INT NULL ,
  mes INT NULL ,
  nome_mes VARCHAR(20) NULL ,
  fim_mes CHAR(3) NULL ,
  trimestre INT NULL ,
  nome_trimestre VARCHAR(20) NULL ,
  semestre INT NULL ,
  nome_semestre VARCHAR(20) NULL ,
  ano INT NULL ,
  estacao VARCHAR(9) NULL ,
  PRIMARY KEY (cod_tempo) );
  
  
CREATE INDEX IN_TEMPO_DATA ON DIM_TEMPO(DATA);


-- -----------------------------------------------------
-- Table dim_pacote
-- -----------------------------------------------------
CREATE  TABLE dim_pacote (
  cod_pacote INT NOT NULL IDENTITY ,
  id_pacote INT NOT NULL ,
  nome_pacote VARCHAR(250) NOT NULL ,
  data_inicio DATETIME NULL ,
  data_fim DATETIME NULL ,
  fl_corrente CHAR(1) NULL ,
  PRIMARY KEY (cod_pacote) );


-- -----------------------------------------------------
-- Table ft_transacao_pacote
-- -----------------------------------------------------
CREATE  TABLE ft_transacao_pacote (
  cod_transacao INT NOT NULL IDENTITY ,
  id_transacao INT NOT NULL ,
  cod_tipo_transacao INT NOT NULL ,
  cod_tipo_pagamento INT NOT NULL ,
  data_transacao INT NOT NULL ,
  cod_faixa_etaria INT NOT NULL ,
  cod_cliente INT NOT NULL ,
  cod_localizacao INT NOT NULL ,
  cod_promocao INT NOT NULL ,
  cod_pacote INT NOT NULL ,
  valor NUMERIC(10,2) NOT NULL ,
  quantidade INT NOT NULL ,
  PRIMARY KEY (cod_transacao) ,
    FOREIGN KEY (cod_tipo_transacao )
    REFERENCES dim_tipo_transacao (cod_tipo_transacao ),
    FOREIGN KEY (cod_tipo_pagamento )
    REFERENCES dim_tipo_pagamento (cod_tipo_pagamento ),
    FOREIGN KEY (data_transacao )
    REFERENCES dim_tempo (cod_tempo ),
    FOREIGN KEY (cod_faixa_etaria )
    REFERENCES dim_faixa_etaria (cod_faixa_etaria ),
    FOREIGN KEY (cod_cliente )
    REFERENCES dim_cliente (cod_cliente ),
    FOREIGN KEY (cod_localizacao )
    REFERENCES dim_localizacao (cod_localizacao ),
    FOREIGN KEY (cod_promocao )
    REFERENCES dim_promocao (cod_promocao ),
    FOREIGN KEY (cod_pacote )
    REFERENCES dim_pacote (cod_pacote ));


-- -----------------------------------------------------
-- Table ft_pagamento_pacote
-- -----------------------------------------------------
CREATE  TABLE ft_pagamento_pacote (
  cod_pagamento INT NOT NULL IDENTITY ,
  id_pagamento INT NOT NULL ,
  cod_tipo_transacao INT NOT NULL ,
  cod_tipo_pagamento INT NOT NULL ,
  data_pagamento INT NOT NULL ,
  cod_faixa_etaria INT NOT NULL ,
  cod_cliente INT NOT NULL ,
  cod_localizacao INT NOT NULL ,
  cod_promocao INT NOT NULL ,
  cod_pacote INT NOT NULL ,
  valor NUMERIC(10,2) NOT NULL ,
  referente_ao_mes INT NOT NULL ,
  quantidade INT NOT NULL ,
  PRIMARY KEY (cod_pagamento) ,
    FOREIGN KEY (cod_tipo_transacao )
    REFERENCES dim_tipo_transacao (cod_tipo_transacao ),
    FOREIGN KEY (cod_tipo_pagamento )
    REFERENCES dim_tipo_pagamento (cod_tipo_pagamento ),
    FOREIGN KEY (data_pagamento )
    REFERENCES dim_tempo (cod_tempo ),
	FOREIGN KEY (referente_ao_mes )
    REFERENCES dim_tempo (cod_tempo ),
    FOREIGN KEY (cod_faixa_etaria )
    REFERENCES dim_faixa_etaria (cod_faixa_etaria ),
    FOREIGN KEY (cod_cliente )
    REFERENCES dim_cliente (cod_cliente ),
    FOREIGN KEY (cod_localizacao )
    REFERENCES dim_localizacao (cod_localizacao ),
    FOREIGN KEY (cod_promocao )
    REFERENCES dim_promocao (cod_promocao ),
    FOREIGN KEY (cod_pacote )
    REFERENCES dim_pacote (cod_pacote ));


-- -----------------------------------------------------
-- Table dim_programa
-- -----------------------------------------------------
CREATE  TABLE dim_programa (
  cod_programa INT NOT NULL IDENTITY ,
  id_programa INT NOT NULL ,
  nome_programa VARCHAR(250) NOT NULL ,
  data_inicio DATETIME NULL ,
  data_fim DATETIME NULL ,
  fl_corrente CHAR(1) NULL ,
  PRIMARY KEY (cod_programa) );


-- -----------------------------------------------------
-- Table dim_categoria_programa
-- -----------------------------------------------------
CREATE  TABLE dim_categoria_programa (
  cod_categoria_programa INT NOT NULL IDENTITY ,
  id_categoria_programa INT NOT NULL ,
  categoria_programa VARCHAR(45) NOT NULL ,
  data_inicio DATETIME NULL ,
  data_fim DATETIME NULL ,
  fl_corrente CHAR(1) NULL ,
  PRIMARY KEY (cod_categoria_programa) );


-- -----------------------------------------------------
-- Table dim_canal
-- -----------------------------------------------------
CREATE  TABLE dim_canal (
  cod_canal INT NOT NULL IDENTITY ,
  id_canal INT NOT NULL ,
  nome_canal VARCHAR(45) NOT NULL ,
  data_inicio DATETIME NULL ,
  data_fim DATETIME NULL ,
  fl_corrente CHAR(1) NULL ,
  PRIMARY KEY (cod_canal) );


-- -----------------------------------------------------
-- Table dim_categoria_canal
-- -----------------------------------------------------
CREATE  TABLE dim_categoria_canal (
  cod_categoria_canal INT NOT NULL IDENTITY ,
  id_categoria_canal INT NOT NULL ,
  categoria_canal VARCHAR(45) NOT NULL ,
  data_inicio DATETIME NULL ,
  data_fim DATETIME NULL ,
  fl_corrente CHAR(1) NULL ,
  PRIMARY KEY (cod_categoria_canal) );


-- -----------------------------------------------------
-- Table ft_transacao_canal
-- -----------------------------------------------------
CREATE  TABLE ft_transacao_canal (
  cod_transacao INT NOT NULL IDENTITY ,
  id_transacao INT NOT NULL ,
  cod_tipo_transacao INT NOT NULL ,
  cod_tipo_pagamento INT NOT NULL ,
  cod_promocao INT NOT NULL ,
  cod_canal INT NOT NULL ,
  cod_categoria_canal INT NOT NULL ,
  data_transacao INT NOT NULL ,
  cod_localizacao INT NOT NULL ,
  cod_cliente INT NOT NULL ,
  cod_faixa_etaria INT NOT NULL ,
  quantidade INT NOT NULL ,
  valor NUMERIC(10,2) NOT NULL ,
  PRIMARY KEY (cod_transacao) ,
    FOREIGN KEY (cod_tipo_transacao )
    REFERENCES dim_tipo_transacao (cod_tipo_transacao ),
    FOREIGN KEY (cod_tipo_pagamento )
    REFERENCES dim_tipo_pagamento (cod_tipo_pagamento ),
    FOREIGN KEY (cod_promocao )
    REFERENCES dim_promocao (cod_promocao ),
    FOREIGN KEY (cod_canal )
    REFERENCES dim_canal (cod_canal ),
    FOREIGN KEY (cod_categoria_canal )
    REFERENCES dim_categoria_canal (cod_categoria_canal ),
    FOREIGN KEY (data_transacao )
    REFERENCES dim_tempo (cod_tempo ),
    FOREIGN KEY (cod_localizacao )
    REFERENCES dim_localizacao (cod_localizacao ),
    FOREIGN KEY (cod_cliente )
    REFERENCES dim_cliente (cod_cliente ),
    FOREIGN KEY (cod_faixa_etaria )
    REFERENCES dim_faixa_etaria (cod_faixa_etaria ));


-- -----------------------------------------------------
-- Table ft_transacao_programa
-- -----------------------------------------------------
CREATE  TABLE ft_transacao_programa (
  cod_transacao INT NOT NULL IDENTITY ,
  id_transacao INT NOT NULL ,
  cod_tipo_transacao INT NOT NULL ,
  cod_tipo_pagamento INT NOT NULL ,
  data_transacao INT NOT NULL ,
  cod_localizacao INT NOT NULL ,
  cod_cliente INT NOT NULL ,
  cod_faixa_etaria INT NOT NULL ,
  cod_programa INT NOT NULL ,
  cod_categoria_programa INT NOT NULL ,
  cod_promocao INT NOT NULL ,
  quantidade INT NOT NULL ,
  valor NUMERIC(10,2) NOT NULL ,
  PRIMARY KEY (cod_transacao) ,
    FOREIGN KEY (cod_tipo_transacao )
    REFERENCES dim_tipo_transacao (cod_tipo_transacao ),
    FOREIGN KEY (cod_tipo_pagamento )
    REFERENCES dim_tipo_pagamento (cod_tipo_pagamento ),
    FOREIGN KEY (data_transacao )
    REFERENCES dim_tempo (cod_tempo ),
    FOREIGN KEY (cod_localizacao )
    REFERENCES dim_localizacao (cod_localizacao ),
    FOREIGN KEY (cod_cliente )
    REFERENCES dim_cliente (cod_cliente ),
    FOREIGN KEY (cod_faixa_etaria )
    REFERENCES dim_faixa_etaria (cod_faixa_etaria ),
    FOREIGN KEY (cod_programa )
    REFERENCES dim_programa (cod_programa ),
    FOREIGN KEY (cod_categoria_programa )
    REFERENCES dim_categoria_programa (cod_categoria_programa ),
    FOREIGN KEY (cod_promocao )
    REFERENCES dim_promocao (cod_promocao ));


-- -----------------------------------------------------
-- Table ft_pagamento_canal
-- -----------------------------------------------------
CREATE  TABLE ft_pagamento_canal (
  cod_pagamento INT NOT NULL IDENTITY ,
  id_pagamento INT NOT NULL ,
  cod_tipo_transacao INT NOT NULL ,
  cod_tipo_pagamento INT NOT NULL ,
  cod_promocao INT NOT NULL ,
  cod_canal INT NOT NULL ,
  cod_categoria_canal INT NOT NULL ,
  data_pagamento INT NOT NULL ,
  cod_localizacao INT NOT NULL ,
  cod_cliente INT NOT NULL ,
  cod_faixa_etaria INT NOT NULL ,
  referente_ao_mes INT NOT NULL ,
  quantidade INT NOT NULL ,
  valor NUMERIC(10,2) NOT NULL ,
  PRIMARY KEY (cod_pagamento) ,
    FOREIGN KEY (cod_tipo_transacao )
    REFERENCES dim_tipo_transacao (cod_tipo_transacao ),
    FOREIGN KEY (cod_tipo_pagamento )
    REFERENCES dim_tipo_pagamento (cod_tipo_pagamento ),
    FOREIGN KEY (cod_promocao )
    REFERENCES dim_promocao (cod_promocao ),
    FOREIGN KEY (cod_canal )
    REFERENCES dim_canal (cod_canal ),
    FOREIGN KEY (cod_categoria_canal )
    REFERENCES dim_categoria_canal (cod_categoria_canal ),
    FOREIGN KEY (data_pagamento )
    REFERENCES dim_tempo (cod_tempo ),
	FOREIGN KEY (referente_ao_mes )
    REFERENCES dim_tempo (cod_tempo ),
    FOREIGN KEY (cod_localizacao )
    REFERENCES dim_localizacao (cod_localizacao ),
    FOREIGN KEY (cod_cliente )
    REFERENCES dim_cliente (cod_cliente ),
    FOREIGN KEY (cod_faixa_etaria )
    REFERENCES dim_faixa_etaria (cod_faixa_etaria ));


-- -----------------------------------------------------
-- Table ft_pagamento_programa
-- -----------------------------------------------------
CREATE  TABLE ft_pagamento_programa (
  cod_pagamento INT NOT NULL IDENTITY ,
  id_pagamento INT NOT NULL ,
  cod_tipo_transacao INT NOT NULL ,
  cod_tipo_pagamento INT NOT NULL ,
  data_pagamento INT NOT NULL ,
  cod_localizacao INT NOT NULL ,
  cod_cliente INT NOT NULL ,
  cod_faixa_etaria INT NOT NULL ,
  cod_programa INT NOT NULL ,
  cod_categoria_programa INT NOT NULL ,
  cod_promocao INT NOT NULL ,
  referente_ao_mes INT NOT NULL ,
  quantidade INT NOT NULL ,
  valor NUMERIC(10,2) NOT NULL ,
  PRIMARY KEY (cod_pagamento) ,
    FOREIGN KEY (cod_tipo_transacao )
    REFERENCES dim_tipo_transacao (cod_tipo_transacao ),
    FOREIGN KEY (cod_tipo_pagamento )
    REFERENCES dim_tipo_pagamento (cod_tipo_pagamento ),
    FOREIGN KEY (data_pagamento )
    REFERENCES dim_tempo (cod_tempo ),
	FOREIGN KEY (referente_ao_mes )
    REFERENCES dim_tempo (cod_tempo ),
    FOREIGN KEY (cod_localizacao )
    REFERENCES dim_localizacao (cod_localizacao ),
    FOREIGN KEY (cod_cliente )
    REFERENCES dim_cliente (cod_cliente ),
    FOREIGN KEY (cod_faixa_etaria )
    REFERENCES dim_faixa_etaria (cod_faixa_etaria ),
    FOREIGN KEY (cod_programa )
    REFERENCES dim_programa (cod_programa ),
    FOREIGN KEY (cod_categoria_programa )
    REFERENCES dim_categoria_programa (cod_categoria_programa ),
    FOREIGN KEY (cod_promocao )
    REFERENCES dim_promocao (cod_promocao ));
    

--AGREGADOS

-- -----------------------------------------------------
-- Table ag_transacao_pacote_ano
-- -----------------------------------------------------
CREATE  TABLE ag_transacao_pacote_ano (
  cod_tipo_transacao INT NOT NULL ,
  cod_tipo_pagamento INT NOT NULL ,
  ano INT NOT NULL ,
  cod_faixa_etaria INT NOT NULL ,
  cod_cliente INT NOT NULL ,
  cod_localizacao INT NOT NULL ,
  cod_promocao INT NOT NULL ,
  cod_pacote INT NOT NULL ,
  valor NUMERIC(10,2) NOT NULL ,
  quantidade INT NOT NULL ,
    FOREIGN KEY (cod_tipo_transacao )
    REFERENCES dim_tipo_transacao (cod_tipo_transacao ),
    FOREIGN KEY (cod_tipo_pagamento )
    REFERENCES dim_tipo_pagamento (cod_tipo_pagamento ),
    FOREIGN KEY (ano )
    REFERENCES dim_tempo (cod_tempo ),
    FOREIGN KEY (cod_faixa_etaria )
    REFERENCES dim_faixa_etaria (cod_faixa_etaria ),
    FOREIGN KEY (cod_cliente )
    REFERENCES dim_cliente (cod_cliente ),
    FOREIGN KEY (cod_localizacao )
    REFERENCES dim_localizacao (cod_localizacao ),
    FOREIGN KEY (cod_promocao )
    REFERENCES dim_promocao (cod_promocao ),
    FOREIGN KEY (cod_pacote )
    REFERENCES dim_pacote (cod_pacote ));


-- -----------------------------------------------------
-- Table ag_pagamento_pacote_ano
-- -----------------------------------------------------
CREATE  TABLE ag_pagamento_pacote_ano (
  cod_tipo_transacao INT NOT NULL ,
  cod_tipo_pagamento INT NOT NULL ,
  ano INT NOT NULL ,
  cod_faixa_etaria INT NOT NULL ,
  cod_cliente INT NOT NULL ,
  cod_localizacao INT NOT NULL ,
  cod_promocao INT NOT NULL ,
  cod_pacote INT NOT NULL ,
  valor NUMERIC(10,2) NOT NULL ,
  quantidade INT NOT NULL ,
    FOREIGN KEY (cod_tipo_transacao )
    REFERENCES dim_tipo_transacao (cod_tipo_transacao ),
    FOREIGN KEY (cod_tipo_pagamento )
    REFERENCES dim_tipo_pagamento (cod_tipo_pagamento ),
    FOREIGN KEY (ano )
    REFERENCES dim_tempo (cod_tempo ),
    FOREIGN KEY (cod_faixa_etaria )
    REFERENCES dim_faixa_etaria (cod_faixa_etaria ),
    FOREIGN KEY (cod_cliente )
    REFERENCES dim_cliente (cod_cliente ),
    FOREIGN KEY (cod_localizacao )
    REFERENCES dim_localizacao (cod_localizacao ),
    FOREIGN KEY (cod_promocao )
    REFERENCES dim_promocao (cod_promocao ),
    FOREIGN KEY (cod_pacote )
    REFERENCES dim_pacote (cod_pacote ));
    
    

-- -----------------------------------------------------
-- Table ag_transacao_canal_ano
-- -----------------------------------------------------
CREATE  TABLE ag_transacao_canal_ano (
  cod_tipo_transacao INT NOT NULL ,
  cod_tipo_pagamento INT NOT NULL ,
  cod_promocao INT NOT NULL ,
  cod_canal INT NOT NULL ,
  cod_categoria_canal INT NOT NULL ,
  ano INT NOT NULL ,
  cod_localizacao INT NOT NULL ,
  cod_cliente INT NOT NULL ,
  cod_faixa_etaria INT NOT NULL ,
  quantidade INT NOT NULL ,
  valor NUMERIC(10,2) NOT NULL ,
    FOREIGN KEY (cod_tipo_transacao )
    REFERENCES dim_tipo_transacao (cod_tipo_transacao ),
    FOREIGN KEY (cod_tipo_pagamento )
    REFERENCES dim_tipo_pagamento (cod_tipo_pagamento ),
    FOREIGN KEY (cod_promocao )
    REFERENCES dim_promocao (cod_promocao ),
    FOREIGN KEY (cod_canal )
    REFERENCES dim_canal (cod_canal ),
    FOREIGN KEY (cod_categoria_canal )
    REFERENCES dim_categoria_canal (cod_categoria_canal ),
    FOREIGN KEY (ano )
    REFERENCES dim_tempo (cod_tempo ),
    FOREIGN KEY (cod_localizacao )
    REFERENCES dim_localizacao (cod_localizacao ),
    FOREIGN KEY (cod_cliente )
    REFERENCES dim_cliente (cod_cliente ),
    FOREIGN KEY (cod_faixa_etaria )
    REFERENCES dim_faixa_etaria (cod_faixa_etaria ));


-- -----------------------------------------------------
-- Table ag_transacao_programa_ano
-- -----------------------------------------------------
CREATE  TABLE ag_transacao_programa_ano (
  cod_tipo_transacao INT NOT NULL ,
  cod_tipo_pagamento INT NOT NULL ,
  ano INT NOT NULL ,
  cod_localizacao INT NOT NULL ,
  cod_cliente INT NOT NULL ,
  cod_faixa_etaria INT NOT NULL ,
  cod_programa INT NOT NULL ,
  cod_categoria_programa INT NOT NULL ,
  cod_promocao INT NOT NULL ,
  quantidade INT NOT NULL ,
  valor NUMERIC(10,2) NOT NULL ,
    FOREIGN KEY (cod_tipo_transacao )
    REFERENCES dim_tipo_transacao (cod_tipo_transacao ),
    FOREIGN KEY (cod_tipo_pagamento )
    REFERENCES dim_tipo_pagamento (cod_tipo_pagamento ),
    FOREIGN KEY (ano )
    REFERENCES dim_tempo (cod_tempo ),
    FOREIGN KEY (cod_localizacao )
    REFERENCES dim_localizacao (cod_localizacao ),
    FOREIGN KEY (cod_cliente )
    REFERENCES dim_cliente (cod_cliente ),
    FOREIGN KEY (cod_faixa_etaria )
    REFERENCES dim_faixa_etaria (cod_faixa_etaria ),
    FOREIGN KEY (cod_programa )
    REFERENCES dim_programa (cod_programa ),
    FOREIGN KEY (cod_categoria_programa )
    REFERENCES dim_categoria_programa (cod_categoria_programa ),
    FOREIGN KEY (cod_promocao )
    REFERENCES dim_promocao (cod_promocao ));


-- -----------------------------------------------------
-- Table ag_pagamento_canal_ano
-- -----------------------------------------------------
CREATE  TABLE ag_pagamento_canal_ano (
  cod_tipo_transacao INT NOT NULL ,
  cod_tipo_pagamento INT NOT NULL ,
  cod_promocao INT NOT NULL ,
  cod_canal INT NOT NULL ,
  cod_categoria_canal INT NOT NULL ,
  ano INT NOT NULL ,
  cod_localizacao INT NOT NULL ,
  cod_cliente INT NOT NULL ,
  cod_faixa_etaria INT NOT NULL ,
  quantidade INT NOT NULL ,
  valor NUMERIC(10,2) NOT NULL ,
    FOREIGN KEY (cod_tipo_transacao )
    REFERENCES dim_tipo_transacao (cod_tipo_transacao ),
    FOREIGN KEY (cod_tipo_pagamento )
    REFERENCES dim_tipo_pagamento (cod_tipo_pagamento ),
    FOREIGN KEY (cod_promocao )
    REFERENCES dim_promocao (cod_promocao ),
    FOREIGN KEY (cod_canal )
    REFERENCES dim_canal (cod_canal ),
    FOREIGN KEY (cod_categoria_canal )
    REFERENCES dim_categoria_canal (cod_categoria_canal ),
    FOREIGN KEY (ano )
    REFERENCES dim_tempo (cod_tempo ),
    FOREIGN KEY (cod_localizacao )
    REFERENCES dim_localizacao (cod_localizacao ),
    FOREIGN KEY (cod_cliente )
    REFERENCES dim_cliente (cod_cliente ),
    FOREIGN KEY (cod_faixa_etaria )
    REFERENCES dim_faixa_etaria (cod_faixa_etaria ));


-- -----------------------------------------------------
-- Table ag_pagamento_programa_ano
-- -----------------------------------------------------
CREATE  TABLE ag_pagamento_programa_ano (
  cod_tipo_transacao INT NOT NULL ,
  cod_tipo_pagamento INT NOT NULL ,
  ano INT NOT NULL ,
  cod_localizacao INT NOT NULL ,
  cod_cliente INT NOT NULL ,
  cod_faixa_etaria INT NOT NULL ,
  cod_programa INT NOT NULL ,
  cod_categoria_programa INT NOT NULL ,
  cod_promocao INT NOT NULL ,
  quantidade INT NOT NULL ,
  valor NUMERIC(10,2) NOT NULL ,
    FOREIGN KEY (cod_tipo_transacao )
    REFERENCES dim_tipo_transacao (cod_tipo_transacao ),
    FOREIGN KEY (cod_tipo_pagamento )
    REFERENCES dim_tipo_pagamento (cod_tipo_pagamento ),
    FOREIGN KEY (ano )
    REFERENCES dim_tempo (cod_tempo ),
    FOREIGN KEY (cod_localizacao )
    REFERENCES dim_localizacao (cod_localizacao ),
    FOREIGN KEY (cod_cliente )
    REFERENCES dim_cliente (cod_cliente ),
    FOREIGN KEY (cod_faixa_etaria )
    REFERENCES dim_faixa_etaria (cod_faixa_etaria ),
    FOREIGN KEY (cod_programa )
    REFERENCES dim_programa (cod_programa ),
    FOREIGN KEY (cod_categoria_programa )
    REFERENCES dim_categoria_programa (cod_categoria_programa ),
    FOREIGN KEY (cod_promocao )
    REFERENCES dim_promocao (cod_promocao ));


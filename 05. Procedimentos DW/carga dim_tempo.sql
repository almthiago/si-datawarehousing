
--inicio procedure
CREATE PROCEDURE SP_ATUALIZA_DIM_TEMPO
(@DATAINICIO DATETIME, @DATAFIM DATETIME)
AS
BEGIN
	SET LANGUAGE BRAZILIAN
	
	DECLARE @DATA DATETIME, @NOMEDIA VARCHAR(10), @dia_util CHAR(3), 
		@FIMDESEMANA CHAR(3), @QUINZENA INT, @fim_mes CHAR(3), @MES INT, 
		@nome_mes VARCHAR(15), @TRIMESTRE INT, @nome_trimestre VARCHAR(50), 
		@SEMESTRE INT, @nome_semestre VARCHAR(50), @ESTACAO VARCHAR(9), @CONT_MES INT, @CONT_ANO INT, @ANO INT, @QTDE_LINHAS INT

	SET @DATA = @DATAINICIO 
	SET @CONT_MES = DATEPART(MONTH,@DATA)-1
	SET @CONT_ANO = DATEPART(YEAR,@DATA)-1

	WHILE(@DATA <= @DATAFIM)
	BEGIN
		SELECT COD_TEMPO FROM DIM_TEMPO 
		WHERE DATA = @DATA
		
		SET @QTDE_LINHAS = @@ROWCOUNT
		
		IF (@QTDE_LINHAS = 0)
		BEGIN		
			IF (@CONT_ANO <> DATEPART(YEAR,@DATA))
			BEGIN
				SET @ANO = DATEPART(YEAR,@DATA)
			END
			EXEC SP_CLASSIFICA_DIA @DATA, @NOMEDIA OUTPUT, @dia_util OUTPUT,
									@FIMDESEMANA OUTPUT, @QUINZENA OUTPUT, @fim_mes OUTPUT
			 
			EXEC SP_CLASSIFICA_MES @DATA, @nome_mes OUTPUT, @TRIMESTRE OUTPUT,
										@nome_trimestre OUTPUT, @SEMESTRE OUTPUT, @nome_semestre OUTPUT
										
			EXEC SP_ESTACAO @DATA, @ESTACAO OUTPUT 
			
			INSERT INTO DIM_Tempo
			(Nivel,Data,Dia,dia_semana,dia_util,fim_semana,Quinzena,Mes,nome_mes,
				fim_mes,Trimestre,nome_trimestre,Semestre,nome_semestre,Ano,Estacao)
			VALUES
			('DIA',@DATA,DAY(@DATA),@NOMEDIA,@dia_util,@FIMDESEMANA,@QUINZENA,MONTH(@DATA),@nome_mes,
				@fim_mes,@TRIMESTRE,@nome_trimestre,@SEMESTRE,@nome_semestre,YEAR(@DATA),@ESTACAO)
			
			IF (@CONT_MES <> DATEPART(MONTH,@DATA))
			BEGIN
				INSERT INTO DIM_Tempo
				(Nivel,Data,Dia,dia_semana,dia_util,fim_semana,Quinzena,Mes,nome_mes,
					fim_mes,Trimestre,nome_trimestre,Semestre,nome_semestre,Ano,Estacao)
				VALUES
				('M�S',NULL,NULL,NULL,NULL,NULL,NULL,MONTH(@DATA),@nome_mes,
					NULL,@TRIMESTRE,@nome_trimestre,@SEMESTRE,@nome_semestre,YEAR(@DATA),NULL)
			END
			
			IF (@CONT_ANO <> DATEPART(YEAR,@DATA))
			BEGIN
				INSERT INTO DIM_Tempo
				(Nivel,Data,Dia,dia_semana,dia_util,fim_semana,Quinzena,Mes,nome_mes,
					fim_mes,Trimestre,nome_trimestre,Semestre,nome_semestre,Ano,Estacao)
				VALUES
				('ANO',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,
					NULL,NULL,NULL,NULL,NULL,YEAR(@DATA),NULL)
			END

			SET @CONT_MES = DATEPART(MONTH,@DATA)
			SET @CONT_ANO = DATEPART(YEAR,@DATA)
			SET @DATA = DATEADD(DAY,1,@DATA)
		END
	END
END
--fim procedure
GO

-- classifica dia
CREATE PROCEDURE SP_CLASSIFICA_DIA
(@DATA DATETIME,@NOMEDIA VARCHAR(10) OUTPUT, @dia_util CHAR(3) OUTPUT,
@FIMDESEMANA CHAR(3) OUTPUT, @QUINZENA INT OUTPUT, @fim_mes CHAR(3) OUTPUT)
AS
BEGIN
	DECLARE @dia_semana INT
	SET @dia_semana = DATEPART(DW,@DATA)

				
	SET @NOMEDIA = DATENAME(DW,CURRENT_TIMESTAMP)
		
	IF (@dia_semana = 1 OR @dia_semana = 7)
		BEGIN
			SET @FIMDESEMANA = 'SIM'
			SET @dia_util = 'N�O'		
		END
	ELSE IF (@dia_semana = 2 OR @dia_semana = 3 OR @dia_semana = 4 OR @dia_semana = 5 OR @dia_semana = 6)
		BEGIN
			SET @FIMDESEMANA = 'NAO'
			SET @dia_util = 'SIM'
		END
	
	IF (DATEPART(D,@DATA)<=15)
		SET @QUINZENA = 1
	ELSE
		SET @QUINZENA = 2
		
	IF (DATEPART(D,@DATA)>=28)
		SET @fim_mes = 'SIM'
	ELSE
		SET @fim_mes = 'N�O'
END
--fim procedure
GO

--classifica mes
CREATE PROCEDURE SP_CLASSIFICA_MES
(@DATA DATETIME, @nome_mes VARCHAR(15) OUTPUT, @TRIMESTRE INT OUTPUT, 
	@nome_trimestre VARCHAR(50) OUTPUT, @SEMESTRE INT OUTPUT, @nome_semestre VARCHAR(50) OUTPUT)
AS
BEGIN
	DECLARE @MES INT
	SET @MES = DATEPART(M,@DATA)
	SET @nome_mes = DATENAME(MM, @DATA)
	
	IF (@MES <= 6)
		BEGIN
			SET @SEMESTRE = 1
			SET @nome_semestre = 'PRIMEIRO SEMESTRE'
			IF (@MES <= 3)
				BEGIN
					SET @TRIMESTRE = 1
					SET @nome_trimestre = 'PRIMEIRO TRIMESTRE'
				END
			ELSE
				BEGIN
					SET @TRIMESTRE = 2
					SET @nome_trimestre = 'SEGUNDO TRIMESTRE'
				END
		END
	ELSE
		BEGIN
			SET @SEMESTRE = 2
			SET @nome_semestre = 'SEGUNDO SEMESTRE'
			IF (@MES <= 9)
				BEGIN
					SET @TRIMESTRE = 3
					SET @nome_trimestre = 'TERCEIRO TRIMESTRE'
				END
			ELSE
				BEGIN
					SET @TRIMESTRE = 4
					SET @nome_trimestre = 'QUARTO TRIMESTRE'
				END
		END
END
--fim procedure classifica mes
GO

--procedure estacao
CREATE PROCEDURE SP_ESTACAO
(@DATA DATETIME, @ESTACAO VARCHAR(9) OUTPUT)
AS
BEGIN
	DECLARE @DIA INT, @MES INT
	SET @DIA = DATEPART(D,@DATA)
	SET @MES = DATEPART(M,@DATA)
	SET @ESTACAO = 'NEM'

	IF (@MES < 3)
		SET @ESTACAO = 'VER�O'
	ELSE IF (@MES > 3 AND @MES < 6) 
		SET @ESTACAO = 'OUTONO'
	ELSE IF (@MES > 6 AND @MES < 9)
		SET @ESTACAO = 'INVERNO'
	ELSE IF (@MES > 9 AND @MES < 12)
		SET @ESTACAO = 'PRIMAVERA'
		
	IF (@MES = 3)
		IF (@DIA >= 21)
			SET @ESTACAO = 'OUTONO'
		ELSE
			SET @ESTACAO = 'VER�O'		
	ELSE IF (@MES = 6)
		IF (@DIA >= 21)
			SET @ESTACAO = 'INVERNO'
		ELSE
			SET @ESTACAO = 'OUTONO'
	ELSE IF (@MES = 9)
		IF (@DIA >= 23)
			SET @ESTACAO = 'PRIMAVERA'
		ELSE
			SET @ESTACAO = 'INVERNO'
	ELSE IF (@MES = 12)
		IF (@DIA >= 21)
			SET @ESTACAO = 'VER�O'
		ELSE
			SET @ESTACAO = 'PRIMAVERA'
END
--fim procedure esta��o

